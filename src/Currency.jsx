import React, {Fragment} from 'react'
import PropTypes from 'prop-types'

const formattedPrice = (num, currency, locale = "it-IT") => {
  if (num && isNumeric(num)) {
    return new Intl.NumberFormat(locale, {
      style: "currency",
      maximumFractionDigits: 2,
      minimumFractionDigits: 2,
      currency: currency
    }).format(num);
  }
  return '';
};

const Currency = props => {
  const formattedValue = formattedPrice(props.num, props.currency, props.locale)
  return (
    <Fragment>
      { formattedValue }
    </Fragment>
  )
}

Currency.propTypes = {

}

export default Currency
